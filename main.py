from merge import combine
from merge import concat

def main():
     global N, closest_ans, closest_seq, target, numbers
     while (True):
         number_str = raw_input('Enter the numbers: ')
         numbers = [int(x) for x in number_str.split()]
         N = len(numbers)

         target = int(raw_input('Enter the target number: '))
         closest_ans = 0

         dfs(0, 0, '0') # unsure if this call is 100% necessary
         for idx in range(N):
             num = numbers[idx]
             dfs(num, (1 << idx), str(num))

         print closest_ans, ':', closest_seq
         print
         cnt = raw_input('Enter Y/N to continue/quit: ')
         print
         if (cnt == 'N' or cnt == 'n'):
             break

# Depth First Search
def dfs(current_sum, bitmask, seq):
    global N, closest_ans, closest_seq, target, numbers
    if (abs(current_sum - target) < abs(closest_ans - target)):
        closest_ans = current_sum
        closest_seq = seq
    if closest_ans == target:
        return

    # Performs an operation on 1 unused number, combining it with current sum.
    for idx in range(N):
        if not ( (1 << idx) & bitmask):
            nxt = numbers[idx]
            nxt_bitmask = (bitmask | (1 << idx))
            for op in range(6):
                (valid, future_sum) = combine(current_sum, nxt, op)
                if valid:
                    nxt_seq = concat(seq, str(nxt), op)
                    dfs(future_sum, nxt_bitmask, nxt_seq)
        
    # Performs an operation on 2 unused numbers, then combines it with current sum.
    # Accounts for situations like: (A * B) + (C * D)
    for idx1 in range(N):
        if not ( (1 << idx1) & bitmask ):
            for idx2 in range(N):
               if ( idx1 != idx2 ) and not ( (1 << idx2) & bitmask ):
                  nxt_bitmask = (bitmask | (1 << idx1) | (1 << idx2))
                  nxt1 = numbers[idx1]
                  nxt2 = numbers[idx2]
                  for op1 in range(6): # operations on 2 numbers
                      (valid1, nxt_total) = combine(nxt1, nxt2, op1)
                      if not valid1: 
                          continue
                      str_total = concat(str(nxt1), str(nxt2), op1)

                      for op2 in range(6): # combining it with current sum
                          (valid2, future_sum) = combine(current_sum, nxt_total, op2)
                          if (valid2):
                              nxt_seq = concat(seq, str_total, op2)
                              dfs(future_sum, nxt_bitmask, nxt_seq)
        
if __name__ == '__main__':
    main()
    
