# Solves the Countdown Numbers game.

### Description:
- Uses a DFS method to brute force all the combinations.
- Able to handle variable numbers of inputs (N).
- Prints the closest number to the target followed by the sequence that forms it.

### Usage:
> python main.py  
Enter the numbers: 75 50 10 8 9 8  
Enter the target number: 985  
985 : (((75 - 10) * 9) + (50 * 8))   
<br />
Enter Y/N to continue/quit: Y  
<br />
Enter the numbers: 3 5 9 25 50  
Enter the target number: 311  
310 : ((((3 * 5) + 25) * 9) - 50)  
<br />
Enter Y/N to continue/quit: N  

### Improvements:
- Speed can be improved by only constructing the final string sequence once. Right now, it constructs a string for every operation.
