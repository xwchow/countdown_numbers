OPCHAR = [
    ' + ', 
    ' - ',
    ' - ', # RSUB
    ' * ',
    ' / ',
    ' / '  # RDIV
]

def combine(sum_1, sum_2, op):
    # handle division by zero
    # only allow addition
    if sum_1 == 0 or sum_2 == 0:
        if op == 0: 
            return (True, sum_1 + sum_2)
        else: 
            return (False, 0)

    # handle invalid situations
    if op == 1 and sum_1 <= sum_2: return (False, 0)
    if op == 2 and sum_2 <= sum_1: return (False, 0)
    if op == 4 and sum_1 % sum_2: return (False, 0)
    if op == 5 and sum_2 % sum_1: return (False, 0)

    # perform operations
    if op == 0: return (True, sum_1 + sum_2)
    if op == 1: return (True, sum_1 - sum_2)
    if op == 2: return (True, sum_2 - sum_1)
    if op == 3: return (True, sum_1 * sum_2)
    if op == 4: return (True, sum_1 / sum_2)
    if op == 5: return (True, sum_2 / sum_1)
    print 'YOU SHOULD NOT BE HERE !'

def concat(str_1, str_2, op):
    # handle zero case
    if str_1 == '0': return str_2
    if str_2 == '0': return str_1

    if op == 2 or op == 5:
        (str_1, str_2) = (str_2, str_1)
    return '(' + str_1 + OPCHAR[op] + str_2 + ')'

def main():
    # just for testing purposes
    print combine(0, 1, 0)

if __name__ == '__main__':
    main()
